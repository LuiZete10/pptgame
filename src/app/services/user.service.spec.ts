import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new UserService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Funcionamiento de UserService', () => {

    beforeEach(()=>{
      localStorage.setItem('PRUEBAUNITARIA2021',JSON.stringify({nombre:"PRUEBAUNITARIA2021",puntuacion:100}));
    })

    afterEach(()=>{
      localStorage.removeItem('PRUEBAUNITARIA2022');
      localStorage.removeItem('PRUEBAUNITARIA2021');
    })

    it('Se valida un usuario valido',() =>{
      expect(service.esValido('Antonio')).toBeTrue();
    });

    it('Se valida un usuario no válido',() =>{
      expect(service.esValido('A.na')).toBeFalse();
    });

    it('Se comprueba la existencia de un usuario no existente',() =>{
      expect(service.existeUsuario('PRUEBAUNITARIA2020')).toBeFalse();
    });

    it('Se comprueba la existencia de un usuario existente',() =>{
      expect(service.existeUsuario('PRUEBAUNITARIA2021')).toBeTrue();
    });

    it('Se crea un usuario',() =>{
      service.crearUsuario('PRUEBAUNITARIA2022');
      expect(localStorage.getItem('PRUEBAUNITARIA2022')!==null).toBeTrue();
      expect(service.usuarioLogeado.nombre).toBe('PRUEBAUNITARIA2022');
      expect(service.usuarioLogeado.puntuacion).toBe(0)
    });

    it('Se obtiene el usuario',() =>{
      service.getUsuario('PRUEBAUNITARIA2021');
      expect(service.usuarioLogeado.nombre).toBe('PRUEBAUNITARIA2021');
      expect(service.usuarioLogeado.puntuacion).toBe(100)
    })

    it('Se actualiza el usuario',() =>{
      service.getUsuario('PRUEBAUNITARIA2021');
      service.updatePuntuacionUsuario(10);
      expect(service.usuarioLogeado.nombre).toBe('PRUEBAUNITARIA2021');
      expect(service.usuarioLogeado.puntuacion).toBe(110)
    })

    it('Se intenta obtener un usuario inexistente',() =>{
      service.getUsuario('PRUEBAUNITARIA2023');
      expect(service.usuarioLogeado.nombre).toBe('');
      expect(service.usuarioLogeado.puntuacion).toBe(0)
    })

  });
});
