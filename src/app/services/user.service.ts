import { Injectable } from '@angular/core';
import { Usuario } from '@interfaces/user.interface';

@Injectable()
export class UserService {

  private _usuarioLogeado : Usuario = {nombre:"", puntuacion:0};
  private _expReg : RegExp = /(.\s)|(.\s.)|(\s.)|(.[^a-zA-Z0-9])|(.[^a-zA-Z0-9].)|([^a-zA-Z0-9].)/;

  constructor() { }

  crearUsuario(nombreUsuario : string) : Usuario{
    const usuario : Usuario ={
      nombre : nombreUsuario,
      puntuacion : 0
    };
    localStorage.setItem(usuario.nombre,JSON.stringify(usuario));
    this._usuarioLogeado = usuario;
    return usuario;
  }

  esValido(usuario : string) : boolean{
    return (usuario.length >= 5 && !this._expReg.test(usuario));
  }

  getUsuario(nomUsuario : string) : Usuario{
    this.loginUsuarioLocalStorage(nomUsuario)
    return this.usuarioLogeado;
  }

  existeUsuario(nomUsuario : string) : boolean{
    return localStorage.getItem(nomUsuario) !== null;
  }

  updatePuntuacionUsuario(incremento : number){
    this._usuarioLogeado.puntuacion += incremento;
    this.updateUsuarioLocalStorage();
  }

  private loginUsuarioLocalStorage(nomUsuario : string) : boolean{
    if(this.existeUsuario(nomUsuario)){
      this._usuarioLogeado = JSON.parse(localStorage.getItem(nomUsuario)!);
      return true;
    }
    return false;
  }

  private updateUsuarioLocalStorage(){
    localStorage.setItem(this._usuarioLogeado.nombre,JSON.stringify(this._usuarioLogeado));
  }

  get usuarioLogeado() : Usuario{
    return {...this._usuarioLogeado};
  }
}
