import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  nombreUsuario : string = '';
  nombreUsuarioComprobado : string = '';
  esError : boolean = false;
  esAviso : boolean = false;

  constructor(private userService : UserService, private router: Router) { }

  comprobarUsuario(){
    this.nombreUsuarioComprobado = this.nombreUsuario;
    this.esAviso = false;
    this.esError = false;
    if(this.nombreUsuario === ""){
      this.esAviso = true;
    }else if(!this.userService.esValido(this.nombreUsuario)){
      this.esError = true;
    }else{
      if(this.userService.existeUsuario(this.nombreUsuario)){
        this.userService.getUsuario(this.nombreUsuario);
      }else{
        this.userService.crearUsuario(this.nombreUsuario);
      }
      this.router.navigate([`/game/${this.userService.usuarioLogeado.nombre}`]);
    }
  }

}
