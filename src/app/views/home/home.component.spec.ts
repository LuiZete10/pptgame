import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let service: UserService;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports:[RouterTestingModule],
      providers: [UserService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(UserService);
    router = TestBed.inject(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Funcionamiento de HomeComponent', () => {
    it('Se escribe un usuario válido y existente',() =>{
      spyOn(service, 'existeUsuario').and.callFake((nomUsuario : string) =>{return true});
      const llamada = spyOn(service, 'getUsuario').and.callFake((nomUsuario : string) =>{return {nombre:"Antonio",puntuacion:10}});
      const redireccion = spyOn(router, 'navigate').and.callFake( resp  =>{return new Promise((resolve,reject)=>{})});
      component.nombreUsuario="Antonio";
      component.comprobarUsuario();
      expect(component.esAviso).toBeFalse();
      expect(component.esError).toBeFalse();
      expect(llamada).toHaveBeenCalled();
      expect(redireccion).toHaveBeenCalled();
    });
    it('Se escribe un usuario válido y no existente',() =>{
      spyOn(service, 'existeUsuario').and.callFake((nomUsuario : string) =>{return false});
      const llamada = spyOn(service, 'crearUsuario').and.callFake((nomUsuario : string) =>{return {nombre:"Pedro",puntuacion:0}});
      const redireccion = spyOn(router, 'navigate').and.callFake( resp  =>{return new Promise((resolve,reject)=>{})});
      component.nombreUsuario="Pedro";
      component.comprobarUsuario();
      expect(component.esAviso).toBeFalse();
      expect(component.esError).toBeFalse();
      expect(llamada).toHaveBeenCalled();
      expect(redireccion).toHaveBeenCalled();
    });
    it('Se escribe un usuario no válido',() =>{
      component.nombreUsuario="A.na";
      component.comprobarUsuario();
      expect(component.esAviso).toBeFalse();
      expect(component.esError).toBeTrue();
    });
    it('No se escribe un usuario',() =>{
      component.nombreUsuario="";
      component.comprobarUsuario();
      expect(component.esAviso).toBeTrue();
      expect(component.esError).toBeFalse();
    });
  });

});
