import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameComponent } from './game.component';
import { GamepadComponent } from './components/gamepad/gamepad.component';
import { ScreenComponent } from './components/screen/screen.component';
import { GameService } from './services/game.service';



@NgModule({
  declarations: [
    GameComponent,
    GamepadComponent,
    ScreenComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    GameComponent
  ],
  providers:[
    GameService
  ]
})
export class GameModule { }
