import { Component, OnInit } from '@angular/core';
import { GameService } from './services/game.service';
import { UserService } from '@services/user.service';
import { Usuario } from '@interfaces/user.interface';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit{

  usuario : Usuario = { nombre:"", puntuacion:0};

  eleccion : number = 0;
  eleccionMaquina : number = 0;

  resultado : number = 0;

  constructor(private gameService : GameService, private userService : UserService, private actRoute : ActivatedRoute, private router : Router) { }

  empezarPartida(eleccion : number){
    this.eleccion = eleccion;

    setTimeout(() => {
      this.eleccionMaquina = this.gameService.eligeMaquina();
    },1000);

    setTimeout(() => {
      this.resultado = this.gameService.evaluarPartida(this.eleccion,this.eleccionMaquina);
      this.refrescarUsuario();
    } ,2500);

    setTimeout(() => this.reiniciar() ,5000);
  }

  reiniciar(){
    this.eleccion = 0;
    this.eleccionMaquina = 0;
    this.resultado = 0;
  }

  refrescarUsuario(){
    this.usuario = this.userService.usuarioLogeado;
  }

  ngOnInit(){
    if(this.usuario.nombre === ""){
      this.actRoute.url.subscribe((resp) => this.usuario.nombre = resp[1].path);
      if(this.userService.existeUsuario(this.usuario.nombre)){
        this.userService.getUsuario(this.usuario.nombre)
        this.refrescarUsuario();
      }else{
        this.router.navigate([`/`]);
      }
    }else{
      this.refrescarUsuario();
    }
  }
}
