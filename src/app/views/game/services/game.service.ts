import { Injectable } from '@angular/core';
import {PIEDRA, PAPEL, TIJERA, EMPATE, GANADO, PERDIDO} from '@constants'
import { UserService } from '@services/user.service';

@Injectable()
export class GameService {

  constructor(private userService : UserService) { }

  eligeMaquina() : number{
    return Math.ceil(Math.random()*3);
  }

  evaluarPartida(eleccionJugador : number, eleccionMaquina : number) : number{
    if(eleccionJugador == eleccionMaquina){
      return EMPATE;
    }else if((eleccionJugador == PAPEL && eleccionMaquina == PIEDRA) ||
          (eleccionJugador == TIJERA && eleccionMaquina == PAPEL) ||
          (eleccionJugador == PIEDRA && eleccionMaquina == TIJERA)){
      this.userService.updatePuntuacionUsuario(10)
      return GANADO;
    }
    this.userService.updatePuntuacionUsuario(-5)
    return PERDIDO;
  }
}
