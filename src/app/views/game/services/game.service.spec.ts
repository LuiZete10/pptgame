import { TestBed } from '@angular/core/testing';

import { UserService } from '@services/user.service';
import { GameService } from './game.service';
import { PIEDRA, PAPEL, TIJERA, GANADO, EMPATE, PERDIDO} from '@constants';

describe('GameService', () => {
  let service: GameService;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({providers: [UserService]});
    userService = new UserService();
    service = new GameService(userService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Funcionamiento del GameService',()=>{
    it('Se evalua una partida que gana el jugador', () => {
      const llamada = spyOn(userService, 'updatePuntuacionUsuario').and.callFake(()=>{});
      const resultado = service.evaluarPartida(PIEDRA, TIJERA);
      expect(resultado).toBe(GANADO);
      expect(llamada).toHaveBeenCalled();
    });

    it('Se evalua una partida que gana el jugador eligiendo papel', () => {
      const llamada = spyOn(userService, 'updatePuntuacionUsuario').and.callFake(()=>{});
      const resultado = service.evaluarPartida(PAPEL, PIEDRA);
      expect(resultado).toBe(GANADO);
      expect(llamada).toHaveBeenCalled();
    });

    it('Se evalua una partida que termina en empate', () => {
      const resultado = service.evaluarPartida(PAPEL, PAPEL);
      expect(resultado).toBe(EMPATE);
    });

    it('Se evalua una partida que pierde el jugador', () => {
      const llamada = spyOn(userService, 'updatePuntuacionUsuario').and.callFake(()=>{});
      const resultado = service.evaluarPartida(TIJERA, PIEDRA);
      expect(resultado).toBe(PERDIDO);
      expect(llamada).toHaveBeenCalled();
    });

    it('La máquina elige aleatoriamente', () => {
      const eleccionMaquina = service.eligeMaquina();
      expect(eleccionMaquina).toBeGreaterThanOrEqual(PIEDRA);
      expect(eleccionMaquina).toBeLessThanOrEqual(TIJERA);
    });
  });
});
