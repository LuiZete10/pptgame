import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { UserService } from '@services/user.service';
import { GameComponent } from './game.component';
import { GameService } from './services/game.service';
import { PIEDRA, GANADO, PAPEL } from '../../app.constants';
import { of } from 'rxjs';

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;
  let userService: UserService;
  let gameService: GameService;
  let router: Router;
  let actRoute: ActivatedRoute;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameComponent ],
      imports:[RouterTestingModule],
      providers: [UserService, GameService, {provide : ActivatedRoute, useValue:{url : of([{path:'game'},{path:'PRUEBAUNITARIA2022'}])}}]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserService);
    gameService = TestBed.inject(GameService);
    router = TestBed.inject(Router);
    actRoute = TestBed.inject(ActivatedRoute);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Funcionamiento del GameComponent',()=>{
    beforeAll(()=>{
      localStorage.setItem('PRUEBAUNITARIA2022',JSON.stringify({nombre:"PRUEBAUNITARIA2022",puntuacion:100}));
    });

    afterAll(()=>{
      localStorage.removeItem('PRUEBAUNITARIA2022');
    });

    it('Se realiza una partida nueva', fakeAsync(() => {
      const llamadaElegir = spyOn(gameService, 'eligeMaquina').and.callFake(()=> {return PIEDRA});
      const llamadaEvaluar = spyOn(gameService, 'evaluarPartida').and.callFake(()=>{return GANADO});
      component.empezarPartida(PAPEL);
      tick(1000);
      expect(component.eleccionMaquina).toBe(PIEDRA);
      tick(1500);
      expect(component.resultado).toBe(GANADO);
      tick(2500);
      fixture.whenStable().then(() => {
        expect(component.eleccion).toBe(0);
        expect(component.eleccionMaquina).toBe(0);
        expect(component.resultado).toBe(0);
        expect(llamadaElegir).toHaveBeenCalled();
        expect(llamadaEvaluar).toHaveBeenCalled();
      })
    }));

    it('Testing onInit con un usuario vacio', () => {
      component.ngOnInit();
      expect(component.usuario.nombre).toBe('PRUEBAUNITARIA2022');
      expect(component.usuario.puntuacion).toBe(100);
    });

    it('Testing onInit con un usuario', () => {
      component.usuario = {nombre:'PRUEBAUNITARIA2022',puntuacion:100};
      component.ngOnInit();
      expect(component.usuario.nombre).toBe('PRUEBAUNITARIA2022');
      expect(component.usuario.puntuacion).toBe(100);
    });
  });
});
