import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GamepadComponent } from './gamepad.component';
import { PIEDRA } from '@constants';

describe('GamepadComponent', () => {
  let component: GamepadComponent;
  let fixture: ComponentFixture<GamepadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GamepadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GamepadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('El usuario elige', () => {
    const llamada = spyOn(component.whenClick,'emit').and.callFake(()=>{})
    component.elige(PIEDRA);
    expect(component.eleccion).toBe(PIEDRA);
    expect(llamada).toHaveBeenCalled();
  });

});
