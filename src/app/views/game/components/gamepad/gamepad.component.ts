import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-gamepad',
  templateUrl: './gamepad.component.html',
  styleUrls: ['./gamepad.component.css']
})
export class GamepadComponent {

  @Input() eleccion : number = 0;
  @Output() whenClick: EventEmitter<number> = new EventEmitter();

  constructor() { }

  elige(eleccion : number){
    this.eleccion = eleccion;
    this.whenClick.emit(eleccion);
  }

}
