import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from '@views/home/home.component';
import { GameComponent } from '@views/game/game.component';

const routes : Routes = [
    {
        path:'',
        component: HomeComponent,
        pathMatch: 'full'
    },{
        path:'game/:usuario',
        component: GameComponent
    },{
        path:'**',
        redirectTo: ''
    }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
