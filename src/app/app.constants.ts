export const PIEDRA : number = 1;
export const PAPEL : number = 2;
export const TIJERA : number = 3;

export const EMPATE : number = 1;
export const GANADO : number = 2;
export const PERDIDO : number = 3;