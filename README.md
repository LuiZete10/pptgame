# PPTGame

**PPTGame** es un proyecto creado para simular el conocido juego de Piedra, Papel o Tijera. El proyecto ha sido creado con [Angular CLI](https://github.com/angular/angular-cli) versión 13.3.2.

## Sobre el proyecto

El proyecto ha sido creado con **TypeScript** y con el framework **Angular**, en su versión 13.3.2. En cuanto al desarrollo se puede ver la evolución con este [grafo](https://gitlab.com/LuiZete10/pptgame/-/network/main) que emula el uso de GitFlow. Para el diseño gráfico se ha usado **Bootstrap** y **CSS**.

En cuanto a la calidad y limpieza del código se ha usado **ESLint** con una configuración por defecto. Se puede revisar la configuración en el archivo .eslintrc.json. 

Se han realizado pruebas unitarias con **Jasmine** en su versión 3.99.1. Se han comprobado los 2 servicios implementados y los 4 componentes realizados. Los resultados son los siguientes:

```bash
TOTAL = 30 SUCCESS

=============================== Coverage summary ===============================
Statements   : 100% ( 100/100 )
Branches     : 100% ( 22/22 )
Functions    : 100% ( 26/26 )
Lines        : 100% ( 88/88 )
================================================================================

```

## ¿Dónde puedo probarlo?

Para probar la versión desplegada puedes acceder a este [link](https://ppt-game-v1.netlify.app) en el que se encuentra la aplicación desplegada en Netlify.

## ¿Cómo puedo probarlo en local?

Descarga el proyecto de [GitLab](https://gitlab.com/LuiZete10/pptgame) y descomprímelo. Acto seguido, abre la consola de comandos y entra en la carpeta resultante.

Contando con que ya cuentas con el Node.js Package Manager (NPM), si no puedes descargarlo [aquí](https://nodejs.org/es/download/), ejecuta el siguiente comando.

```bash
npm install
```

Después de su ejecución, habrás instalado todos los paquetes necesarios, y podrás ejecutar la aplicación en local simplemente ejecutando el ultimo comando, que ejecuta la aplicación en el [localhost](http://localhost:4200/), puerto 4200.

```bash
ng serve
```

## ¿Cómo puedo ejecutar las pruebas?

Contando con que ya se ha ejecutado el proyecto en local, simplemente ejecutando cualquiera de estos 3 comandos:
```bash
npm run test
```
```bash
npm run test:cmd
```
```bash
npm run test:coverage
```

## ¿Cómo puedo comprobar que el código cumple con las reglas establecidas?

Contando con que ya se ha ejecutado el proyecto en local, puedes ejecutar el siguiente comando:
```bash
npm run lint
```